    //==========Add=========
    private Font font2 = new Font(this.getFont().getFontName(), Font.BOLD, 16);  //标记行
	int which;
	//按钮形状
	private String[] types = {"playLast", "playNext", "lyricShow", "lyricHide",
			"orderPlay", "playButton", "pauseButton", "nextButton"};
            
    //=======replace========
    
        paintPlayButton(g2D, 50, 150);
		paintPauseButton(g2D, 50, 250);
		paintNextButton(g2D, 50, 350);
//		for (int i = 0; i < type.length - 1; i++) {
//			createImage(type[i]);
//		}
		
		which = 7;
		boolean isDraw = true;
//		isDraw = false;
		if (isDraw) {
			createImage(types[which]);
		}
        
        //替换 createImage();
        
    //=========Add=========
    /**
	 * 绘画弧形
	 * 
	 * @param g
	 *            Graphics对象
	 * @param x
	 *            左上角x坐标
	 * @param y
	 *            左上角y坐标
	 * @param width
	 * 			      弧形宽
	 * @param height
	 * 			      弧形高
	 * @param startAngle
	 *	    	      起始角度
	 * @param arcAngle
	 * 			      覆盖角度
	 * @param isFill
	 * 			      是否填充：ture 填充；false 空心
	 * @param color
	 *            画笔颜色
	 */
	private void paintArc(Graphics2D g, int x, int y, int width,
			int height, int startAngle, int arcAngle, boolean isFill,
			Color color) {
		g.setColor(color);
		if (isFill) {
			g.fillArc(x, y, width, height, startAngle, arcAngle);
		} else {
			g.drawArc(x, y, width, height, startAngle, arcAngle);
		}
	}


    //绘播放按钮
    public void paintPlayButton(Graphics2D g, int x, int y) {
		int radius = 96;
		int startAngle = 0;
		int arcAngle = 180;
		Color color = new Color(0xff80bd18);
		paintArc(g, x, y, radius, radius, startAngle, arcAngle, true,
				Color.BLACK);
		Arc2D.Float arc1 = new Arc2D.Float(x, y, radius, radius, startAngle, arcAngle, Arc2D.CHORD);
		Arc2D.Float arc2 = new Arc2D.Float(x, y - radius / 2, radius - 2, radius - 2, 180, arcAngle, 1);
		Area area1 = new Area(arc1);
		Area area2 = new Area(arc2);
		area1.intersect(area2);
		g.setColor(color);
		g.fill(area1);
		BasicStroke s = new BasicStroke(2f);
		g.setStroke(s);
		paintArc(g, x - 1, y - 1, radius, radius, startAngle, arcAngle, false,
				Color.BLACK);
		g.clearRect(x - 2, y + radius / 2, radius + 4, 2);
		int[] xPoints = { x + 42, x + 42,
				x + 54 };
		int[] yPoints = { y + 16, y + 32, y + 24 };
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
	}
	
	//绘暂停按钮
	public void paintPauseButton(Graphics2D g, int x, int y) {
		int radius = 96;
		int startAngle = 0;
		int arcAngle = 180;
		Color color = new Color(0xff80bd18);
		paintArc(g, x, y, radius, radius, startAngle, arcAngle, true,
				Color.BLACK);
		Arc2D.Float arc1 = new Arc2D.Float(x, y, radius, radius, startAngle, arcAngle, Arc2D.CHORD);
		Arc2D.Float arc2 = new Arc2D.Float(x, y - radius / 2, radius - 2, radius - 2, 180, arcAngle, 1);
		Area area1 = new Area(arc1);
		Area area2 = new Area(arc2);
		area1.intersect(area2);
		g.setColor(color);
		g.fill(area1);
		BasicStroke s = new BasicStroke(2f);
		g.setStroke(s);
		paintArc(g, x - 1, y - 1, radius, radius, startAngle, arcAngle, false,
				Color.BLACK);
		g.clearRect(x - 2, y + radius / 2, radius + 4, 2);
		g.setColor(Color.WHITE);
		g.fillRect(x + 41, y + 16, 5, 16);
		g.fillRect(x + 50, y + 16, 5, 16);
	}
	
	
	//绘下一曲简洁按钮
	public void paintNextButton(Graphics2D g, int x, int y){
		int[] xPoints = { x + 18, x + 18,
				x + 30 };
		int[] yPoints = { y + 16, y + 32, y + 24 };
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
		g.fillRect(x + 30, y + 16, 4, 16);
	}
    
    //========Alert========
    // 生成图片
	private void createImage(String type) {
		int width;
		int height;
		String imageName;
		switch (which) {
		case 0:
			width = 8;
			height = 24;
			width = ((int) (height * Math.sqrt(2))) * 2 - width;
			imageName = "last";
			break;
		case 1:
			width = 8;
			height = 24;
			width = ((int) (height * Math.sqrt(2))) * 2 - width;
			imageName = "next";
			break;
		case 2:
			width = 108;
			height = 48;
			imageName = "lyric_show";
			break;
		case 3:
			width = 108;
			height = 48;
			imageName = "lyric_hide";
			break;
		case 4:
			width = 108;
			height = 48;
			imageName = "order_play";
			break;
		case 5:
			width = 100;
			height = 50;
			imageName = "play_button";
			break;
		case 6:
			width = 100;
			height = 50;
			imageName = "pause_button";
			break;
		case 7:
			width = 48;
			height = 48;
			imageName = "next_button";
			break;
		default:
				return;
		}
		String src = "D:/source/UI";
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g2D = image.createGraphics();
		image = g2D.getDeviceConfiguration().createCompatibleImage(width,
				height, Transparency.TRANSLUCENT);
		g2D = image.createGraphics();
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2D.setColor(Color.gray);
		g2D.setStroke(new BasicStroke(1));
		switch (which) {
		case 0:
			paintPlayLast(g2D, 0, 0, true, Color.gray);
			break;
		case 1:
			paintPlayNext(g2D, 0, 0, true, Color.gray);
			break;
		case 2:
			paintLyricShow(g2D, 0, 6);
			break;
		case 3:
			paintLyricHide(g2D, 0, 6);
			break;
		case 4:
			paintOrderPlay(g2D, 0, 6);
			break;
		case 5:
			paintPlayButton(g2D, 3, 2);
			break;
		case 6:
			paintPauseButton(g2D, 3, 2);
			break;
		case 7:
			paintNextButton(g2D, 0, 0);
			break;
		} 
		g2D.dispose(); //释放资源
		try {
			ImageIO.write(image, "png",
					new File(src + "/" + imageName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}