package com.acoe.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class ImageUtils extends JPanel{
 
    private static final long serialVersionUID = 1L;
	private Color selfGreen = new Color(0xff9ac718);
	private Font font = new Font(this.getFont().getFontName(), Font.BOLD, 18);
	private Font font2 = new Font(this.getFont().getFontName(), Font.BOLD, 16);
	private Font font3 = new Font(this.getFont().getFontName(), Font.BOLD, 12);
	int which;
	//按钮形状
	private String[] types = {"playLast", "playNext", "lyricShow", "lyricHide",
			"orderPlay", "playButton", "pauseButton", "nextButton", "menubutton",
			"extendMenu", "repeatPlay"};
	

	public ImageUtils() {
		try{
			if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			}
		} catch (Exception e) {
			System.out.println("设置感官界面异常");
			e.printStackTrace();
		}
		JFrame jf = new JFrame("图形生成工具");
		jf.setSize(840, 600);
		try {
			jf.setIconImage(ImageIO.read(ImageUtils.class.getResource("icon.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
//		jf.setUndecorated(true); //不显示边框
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.requestFocus();
		jf.setContentPane(this);
		jf.setVisible(true);
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D g2D = (Graphics2D)g ;
		//去锯齿
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING , RenderingHints.VALUE_ANTIALIAS_ON);
		paintRect(g2D, 0, 0, 200, 100, true, Color.gray);
		paintCircle(g2D, 200, 100, 50, true, Color.blue);
		
		int[] xPoints = {300, 300, (int) (300 + 100 * Math.sqrt(2) / 2)};
		int[] yPoints = {100, 200, 150};
		int nPoints = 3;
		paintPolygon(g2D, xPoints, yPoints, nPoints, true, Color.green);
		
		paintPlayLast(g2D, 400, 300, true, Color.gray);
		paintPlayNext(g2D, 500, 300, true, Color.gray);
		paintLyricShow(g2D, 400, 350);
		paintLyricHide(g2D, 400, 400);
		
		paintOrderPlay(g2D, 400, 450);
		paintRepeatPlay(g2D, 280, 450);
        paintPlayButton(g2D, 50, 150);
		paintPauseButton(g2D, 50, 250);
		paintNextButton(g2D, 50, 350);
		paintMenuButton(g2D, 50, 450);
		paintExtendMenu(g2D, 250, 0);
		
		which = 4;
		boolean isDraw = true;
//		isDraw = false; //不生成图片

		if (isDraw) {
			createImage(types[which]);
		}
	}
	
	/**
	 * 绘画矩形
	 * @param g Graphics对象
	 * @param x 左上角x坐标
	 * @param y 左上角y坐标
	 * @param width 宽
	 * @param height 高
	 * @param isFill 是否填充：ture 填充；false 空心
	 * @param color 画笔颜色
	 */
	private void paintRect(Graphics2D g, int x, int y, int width, int height, boolean isFill, Color color) {
		g.setColor(color);
		if (isFill) {
			g.fillRect(x, y, width, height);
		} else {
			g.drawRect(x, y, width, height);
		}
	}
	
	/**
	 * 绘画圆形
	 * @param g Graphics对象
	 * @param x 左上角x坐标
	 * @param y 左上角y坐标
	 * @param radius 直径
	 * @param isFill 是否填充：ture 填充；false 空心
	 * @param color 画笔颜色
	 */
	private void paintCircle(Graphics2D g, int x, int y, int radius, boolean isFill, Color color) {
		g.setColor(color);
		if (isFill) {
			g.fillArc(x, y, radius, radius, 0, 360);
		} else {
			g.drawArc(x, y, radius, radius, 0, 360);
		}
	}
	
	/**
	 * 绘制多边形
	 * @param g Graphics对象
	 * @param xPoints 顶点x坐标数组
	 * @param yPoints 顶点y坐标数组
	 * @param nPoints 顶点数
	 * @param isFill 是否填充：ture 填充；false 空心
	 * @param color 画笔颜色
	 */
	private void paintPolygon(Graphics2D g, int[] xPoints, int[] yPoints, int nPoints, boolean isFill, Color color) {
		g.setColor(color);
		if (isFill) {
			g.fillPolygon(xPoints, yPoints, nPoints);
		} else {
			g.drawPolygon(xPoints, yPoints, nPoints);
		}
	}
	/**
	 * 绘制字符串
	 * @param g Graphics对象
	 * @param x 左上角x坐标
	 * @param y 左上角y坐标
	 * @param str 字符串
	 * @param font 字体
	 * @param color 字体颜色
	 */
	private void paintString(Graphics2D g, int x, int y, String str, Font font, Color color) {
		g.setColor(color);
		g.setFont(font);
		g.drawString(str, x, y);
	}
	
	public static void main(String[] args) {
		new ImageUtils();
//		imageUtil.repaint();
	}
	
	//绘出“上一首”按钮
	public void paintPlayLast(Graphics2D g, int x, int y, boolean isFill, Color color) {
		//x=400, y=300
		int width = 8;
		int height = 24;
		paintRect(g, x, y, width, height, true, color);
		int[] xPoints = {x + width, x + (int)(height * Math.sqrt(2)), x + (int)(height * Math.sqrt(2))};
		int[] yPoints = {y + height /2, y, y + height};
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, isFill, color);
		xPoints[0] = xPoints[1];
		xPoints[1] = xPoints[2] = xPoints[0] + (int)(height * Math.sqrt(2)) - width;
		paintPolygon(g, xPoints, yPoints, nPoints, isFill, color);
	}
	
	//绘出下一首按钮
	public void paintPlayNext(Graphics2D g, int x, int y, boolean isFill, Color color){
		int width = 8;
		int height = 24;
		int[] xPoints = {x, x, x + (int)(height * Math.sqrt(2)) - width};
		int[] yPoints = {y, y + height, y + height /2};
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, isFill, color);
		xPoints[0] = xPoints[1] = xPoints[2];
		xPoints[2] = xPoints[0] + (int)(height * Math.sqrt(2)) - width;
		paintPolygon(g, xPoints, yPoints, nPoints, isFill, color);
		paintRect(g, xPoints[2], yPoints[0], width, height, true, color);
	}
	
	//绘出歌词显示按钮
	public void paintLyricShow(Graphics2D g,int x, int y) {
		int radius = 36;
		paintCircle(g, x, y, radius, true, Color.LIGHT_GRAY);
		paintRect(g, x + radius / 2, y, radius * 2, radius, true, Color.LIGHT_GRAY);
		paintCircle(g, x + radius / 2 * 3 + radius / 6, y - radius / 6, radius + radius / 3, true, selfGreen);
		font = new Font(this.getFont().getFontName(), Font.BOLD, 18);
		paintString(g, x + radius / 2 - 2, y + 25, "Lyric", font, Color.WHITE);
		paintString(g, x + radius / 2 * 4 - 9, y + 25, "show", font2, Color.WHITE);
	}
	
	//绘出歌词隐藏按钮
	public void paintLyricHide(Graphics2D g,int x, int y) {
		int radius = 36;
		paintCircle(g, x, y, radius, true, Color.LIGHT_GRAY);
		paintRect(g, x + radius / 2, y, radius * 2, radius, true, Color.LIGHT_GRAY);
		paintCircle(g, x + radius / 2 * 3 + radius / 6, y - radius / 6, radius + radius / 3, true, Color.DARK_GRAY);
		paintString(g, x + radius / 2 - 2, y + 25, "Lyric", font, Color.WHITE);
		paintString(g, x + radius / 2 * 4 - 4, y + 25, "hide", font2, Color.WHITE);
	}
	
	//绘出顺序播放按钮
	public void paintOrderPlay(Graphics2D g,int x, int y) {
		int radius = 36;
		paintCircle(g, x + radius * 3 - radius, y, radius, true, Color.LIGHT_GRAY);
		paintRect(g, x + radius - radius / 2, y, radius * 2, radius, true, Color.LIGHT_GRAY);
		paintCircle(g, x, y - radius / 6, radius + radius / 3, true, selfGreen);
		//绘顺序图形
		paintRect(g, x + 9, y - radius / 6 + 24 - 8, 30 - 6, 3, true, Color.white);
		paintRect(g, x + 9, y - radius / 6 + 24 - 2, 30, 3, true, Color.white);
		paintRect(g, x + 9, y - radius / 6 + 24 + 4, 30, 3, true, Color.white);
		int[] xPoints = {x + 9 + 30 - 6, x + 9 + 30 - 6, x + 9 + 30};
		int[] yPoints = {y - radius / 6 + 24 - 8 - 3, y - radius / 6 + 24 - 8 + 3, y - radius / 6 + 24 - 8 + 3};
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
//		paintString(g, x + radius + radius / 3 + 4, y + 25, "Order", font, Color.WHITE);
	}
	
    /**
	 * 绘画弧形
	 * 
	 * @param g
	 *            Graphics对象
	 * @param x
	 *            左上角x坐标
	 * @param y
	 *            左上角y坐标
	 * @param width
	 * 			      弧形宽
	 * @param height
	 * 			      弧形高
	 * @param startAngle
	 *	    	      起始角度
	 * @param arcAngle
	 * 			      覆盖角度
	 * @param isFill
	 * 			      是否填充：ture 填充；false 空心
	 * @param color
	 *            画笔颜色
	 */
	private void paintArc(Graphics2D g, int x, int y, int width,
			int height, int startAngle, int arcAngle, boolean isFill,
			Color color) {
		g.setColor(color);
		if (isFill) {
			g.fillArc(x, y, width, height, startAngle, arcAngle);
		} else {
			g.drawArc(x, y, width, height, startAngle, arcAngle);
		}
	}


    //绘播放按钮
    public void paintPlayButton(Graphics2D g, int x, int y) {
		int radius = 96;
		int startAngle = 0;
		int arcAngle = 180;
		Color color = new Color(0xff80bd18);
		paintArc(g, x, y, radius, radius, startAngle, arcAngle, true,
				Color.BLACK);
		Arc2D.Float arc1 = new Arc2D.Float(x, y, radius, radius, startAngle, arcAngle, Arc2D.CHORD);
		Arc2D.Float arc2 = new Arc2D.Float(x, y - radius / 2, radius - 2, radius - 2, 180, arcAngle, 1);
		Area area1 = new Area(arc1);
		Area area2 = new Area(arc2);
		area1.intersect(area2);
		g.setColor(color);
		g.fill(area1);
		BasicStroke s = new BasicStroke(2f);
		g.setStroke(s);
		paintArc(g, x - 1, y - 1, radius, radius, startAngle, arcAngle, false,
				Color.BLACK);
		g.clearRect(x - 2, y + radius / 2, radius + 4, 2);
		int[] xPoints = { x + 42, x + 42,
				x + 54 };
		int[] yPoints = { y + 16, y + 32, y + 24 };
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
	}
	
	//绘暂停按钮
	public void paintPauseButton(Graphics2D g, int x, int y) {
		int radius = 96;
		int startAngle = 0;
		int arcAngle = 180;
		Color color = new Color(0xff80bd18);
		paintArc(g, x, y, radius, radius, startAngle, arcAngle, true,
				Color.BLACK);
		Arc2D.Float arc1 = new Arc2D.Float(x, y, radius, radius, startAngle, arcAngle, Arc2D.CHORD);
		Arc2D.Float arc2 = new Arc2D.Float(x, y - radius / 2, radius - 2, radius - 2, 180, arcAngle, 1);
		Area area1 = new Area(arc1);
		Area area2 = new Area(arc2);
		area1.intersect(area2);
		g.setColor(color);
		g.fill(area1);
		BasicStroke s = new BasicStroke(2f);
		g.setStroke(s);
		paintArc(g, x - 1, y - 1, radius, radius, startAngle, arcAngle, false,
				Color.BLACK);
		g.clearRect(x - 2, y + radius / 2, radius + 4, 2);
		g.setColor(Color.WHITE);
		g.fillRect(x + 41, y + 16, 5, 16);
		g.fillRect(x + 50, y + 16, 5, 16);
	}
	
	
	//绘下一曲简洁按钮
	public void paintNextButton(Graphics2D g, int x, int y){
		int[] xPoints = { x + 18, x + 18,
				x + 30 };
		int[] yPoints = { y + 16, y + 32, y + 24 };
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
		g.fillRect(x + 30, y + 16, 4, 16);
	}
	
	//绘菜单按钮
	public void paintMenuButton(Graphics2D g, int x, int y){
		BasicStroke s = new BasicStroke(2f);
		g.setStroke(s);
		Color color = new Color(0xff80c014);
		g.setColor(color);
		g.fillRect(x + 20, y + 18, 20, 6);
		g.fillRect(x + 20, y + 27, 20, 6);
		g.fillRect(x + 20, y + 36, 20, 6);
		g.setColor(Color.DARK_GRAY);
		g.drawLine(x + 60, y + 5, x + 60, y + 55);
	}
	
	//绘竖三圆点按钮
	public void paintExtendMenu(Graphics2D g, int x, int y){
		BasicStroke s = new BasicStroke(1f);
		g.setStroke(s);
		paintCircle(g, x + 26, y + 15, 8, true, Color.GRAY);
		paintCircle(g, x + 26, y + 26, 8, true, Color.GRAY);
		paintCircle(g, x + 26, y + 37, 8, true, Color.GRAY);
	}
	
	//绘重复播放按钮
	public void paintRepeatPlay(Graphics2D g, int x, int y){
		int radius = 36;
		paintCircle(g, x + radius * 3 - radius, y, radius, true, Color.LIGHT_GRAY);
		paintRect(g, x + radius - radius / 2, y, radius * 2, radius, true, Color.LIGHT_GRAY);
		paintCircle(g, x, y - radius / 6, radius + radius / 3, true, selfGreen);
		//绘重复图形
		BasicStroke s = new BasicStroke(4f);
		g.setStroke(s);
//		paintArc(g, x + 9, y + 9, 30, 18, 45, 240, false, Color.WHITE);
		g.setColor(Color.WHITE);
		g.drawRoundRect(x + 9, y + 11, 30, 14, 3, 3);
		g.setColor(selfGreen);
		g.fillRect(x + 9 + 18, y + 14, 22, 15);
		g.fillRect(x + 9 + 24, y + 8, 10, 12);
		
		paintCircle(g, x + 9 + 20, y + 11 + 8, 11, true, Color.WHITE);
		g.setColor(selfGreen);
		g.setFont(font3);
		g.drawString("1", x + 9 + 22, y + 11 + 8 + 10);
		
		int[] xPoints = {x + 18 + 15, x + 18 + 15, x + 18 + 8 + 15};
		int[] yPoints = {y + 6, y + 6 + 12, y + 6 + 6};
		int nPoints = 3;
		paintPolygon(g, xPoints, yPoints, nPoints, true, Color.WHITE);
	
	}
	
    // 生成图片
	private void createImage(String type) {
		int width;
		int height;
		String imageName;
		switch (which) {
		case 0:
			width = 8;
			height = 24;
			width = ((int) (height * Math.sqrt(2))) * 2 - width;
			imageName = "last";
			break;
		case 1:
			width = 8;
			height = 24;
			width = ((int) (height * Math.sqrt(2))) * 2 - width;
			imageName = "next";
			break;
		case 2:
			width = 108;
			height = 48;
			imageName = "lyric_show";
			break;
		case 3:
			width = 108;
			height = 48;
			imageName = "lyric_hide";
			break;
		case 4:
			width = 108;
			height = 48;
			imageName = "order_play";
			break;
		case 5:
			width = 100;
			height = 50;
			imageName = "play_button";
			break;
		case 6:
			width = 100;
			height = 50;
			imageName = "pause_button";
			break;
		case 7:
			width = 48;
			height = 48;
			imageName = "next_button";
			break;
		case 8:
			width = 60;
			height = 60;
			imageName = "menu_button";
			break;
		case 9:
			width = 60;
			height = 60;
			imageName = "extend_menu";
			break;
		case 10:
			width = 108;
			height = 48;
			imageName = "repeat_play";
			break;
		default:
				return;
		}
		String src = "D:/UI/AcoeMusic";
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g2D = image.createGraphics();
		image = g2D.getDeviceConfiguration().createCompatibleImage(width,
				height, Transparency.TRANSLUCENT);
		g2D = image.createGraphics();
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2D.setColor(Color.gray);
		g2D.setStroke(new BasicStroke(1));
		switch (which) {
		case 0:
			paintPlayLast(g2D, 0, 0, true, Color.gray);
			break;
		case 1:
			paintPlayNext(g2D, 0, 0, true, Color.gray);
			break;
		case 2:
			paintLyricShow(g2D, 0, 6);
			break;
		case 3:
			paintLyricHide(g2D, 0, 6);
			break;
		case 4:
			paintOrderPlay(g2D, 0, 6);
			break;
		case 5:
			paintPlayButton(g2D, 3, 2);
			break;
		case 6:
			paintPauseButton(g2D, 3, 2);
			break;
		case 7:
			paintNextButton(g2D, 0, 0);
			break;
		case 8:
			paintMenuButton(g2D, 0, 0);
			break;
		case 9:
			paintExtendMenu(g2D, 0, 0);
			break;
		case 10:
			paintRepeatPlay(g2D, 0, 6);
			break;
		} 
		g2D.dispose(); //释放资源

		try {
			ImageIO.write(image, "png",
					new File(src + "/" + imageName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
