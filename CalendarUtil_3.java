
        public int getGregorianYear() {
                return gregorianYear;
        }


        public void setGregorianYear(int gregorianYear) {
                this.gregorianYear = gregorianYear;
        }


        public int getGregorianMonth() {
                return gregorianMonth;
        }


        public void setGregorianMonth(int gregorianMonth) {
                this.gregorianMonth = gregorianMonth;
        }


        public int getGregorianDate() {
                return gregorianDate;
        }


        public void setGregorianDate(int gregorianDate) {
                this.gregorianDate = gregorianDate;
        }


        public boolean isGregorianLeap() {
                return isGregorianLeap;
        }


        public void setGregorianLeap(boolean isGregorianLeap) {
                this.isGregorianLeap = isGregorianLeap;
        }


        public int getDayOfYear() {
                return dayOfYear;
        }


        public void setDayOfYear(int dayOfYear) {
                this.dayOfYear = dayOfYear;
        }


        public int getDayOfWeek() {
                return dayOfWeek;
        }


        public void setDayOfWeek(int dayOfWeek) {
                this.dayOfWeek = dayOfWeek;
        }


        public int getChineseYear() {
                return chineseYear;
        }


        public void setChineseYear(int chineseYear) {
                this.chineseYear = chineseYear;
        }


        public int getChineseMonth() {
                return chineseMonth;
        }


        public void setChineseMonth(int chineseMonth) {
                this.chineseMonth = chineseMonth;
        }


        public int getChineseDate() {
                return chineseDate;
        }


        public void setChineseDate(int chineseDate) {
                this.chineseDate = chineseDate;
        }


        public int getSectionalTerm() {
                return sectionalTerm;
        }


        public void setSectionalTerm(int sectionalTerm) {
                this.sectionalTerm = sectionalTerm;
        }


        public int getPrincipleTerm() {
                return principleTerm;
        }


        public void setPrincipleTerm(int principleTerm) {
                this.principleTerm = principleTerm;
        }


        public static char[] getDaysInGregorianMonth() {
                return daysInGregorianMonth;
        }


        public static void setDaysInGregorianMonth(char[] daysInGregorianMonth) {
                CalendarUtil.daysInGregorianMonth = daysInGregorianMonth;
        }


        public static String[] getStemNames() {
                return stemNames;
        }


        public static void setStemNames(String[] stemNames) {
                CalendarUtil.stemNames = stemNames;
        }


        public static String[] getBranchNames() {
                return branchNames;
        }


        public static void setBranchNames(String[] branchNames) {
                CalendarUtil.branchNames = branchNames;
        }


        public static String[] getAnimalNames() {
                return animalNames;
        }


        public static void setAnimalNames(String[] animalNames) {
                CalendarUtil.animalNames = animalNames;
        }


        public static char[] getChineseMonths() {
                return chineseMonths;
        }


        public static void setChineseMonths(char[] chineseMonths) {
                CalendarUtil.chineseMonths = chineseMonths;
        }


        public static int getBaseYear() {
                return baseYear;
        }


        public static void setBaseYear(int baseYear) {
                CalendarUtil.baseYear = baseYear;
        }


        public static int getBaseMonth() {
                return baseMonth;
        }


        public static void setBaseMonth(int baseMonth) {
                CalendarUtil.baseMonth = baseMonth;
        }


        public static int getBaseDate() {
                return baseDate;
        }


        public static void setBaseDate(int baseDate) {
                CalendarUtil.baseDate = baseDate;
        }


        public static int getBaseIndex() {
                return baseIndex;
        }


        public static void setBaseIndex(int baseIndex) {
                CalendarUtil.baseIndex = baseIndex;
        }


        public static int getBaseChineseYear() {
                return baseChineseYear;
        }


        public static void setBaseChineseYear(int baseChineseYear) {
                CalendarUtil.baseChineseYear = baseChineseYear;
        }


        public static int getBaseChineseMonth() {
                return baseChineseMonth;
        }


        public static void setBaseChineseMonth(int baseChineseMonth) {
                CalendarUtil.baseChineseMonth = baseChineseMonth;
        }


        public static int getBaseChineseDate() {
                return baseChineseDate;
        }


        public static void setBaseChineseDate(int baseChineseDate) {
                CalendarUtil.baseChineseDate = baseChineseDate;
        }


        public static int[] getBigLeapMonthYears() {
                return bigLeapMonthYears;
        }


        public static void setBigLeapMonthYears(int[] bigLeapMonthYears) {
                CalendarUtil.bigLeapMonthYears = bigLeapMonthYears;
        }


        public static char[][] getSectionalTermMap() {
                return sectionalTermMap;
        }


        public static void setSectionalTermMap(char[][] sectionalTermMap) {
                CalendarUtil.sectionalTermMap = sectionalTermMap;
        }


        public static char[][] getSectionalTermYear() {
                return sectionalTermYear;
        }


        public static void setSectionalTermYear(char[][] sectionalTermYear) {
                CalendarUtil.sectionalTermYear = sectionalTermYear;
        }


        public static char[][] getPrincipleTermMap() {
                return principleTermMap;
        }


        public static void setPrincipleTermMap(char[][] principleTermMap) {
                CalendarUtil.principleTermMap = principleTermMap;
        }


        public static char[][] getPrincipleTermYear() {
                return principleTermYear;
        }


        public static void setPrincipleTermYear(char[][] principleTermYear) {
                CalendarUtil.principleTermYear = principleTermYear;
        }


        public static String[] getMonthNames() {
                return monthNames;
        }


        public static void setMonthNames(String[] monthNames) {
                CalendarUtil.monthNames = monthNames;
        }


        public static String[] getChineseMonthNames() {
                return chineseMonthNames;
        }


        public static void setChineseMonthNames(String[] chineseMonthNames) {
                CalendarUtil.chineseMonthNames = chineseMonthNames;
        }


        public static String[] getPrincipleTermNames() {
                return principleTermNames;
        }


        public static void setPrincipleTermNames(String[] principleTermNames) {
                CalendarUtil.principleTermNames = principleTermNames;
        }


        public static String[] getSectionalTermNames() {
                return sectionalTermNames;
        }


        public static void setSectionalTermNames(String[] sectionalTermNames) {
                CalendarUtil.sectionalTermNames = sectionalTermNames;
        }


        public static void main(String[] arg) {
                CalendarUtil c = new CalendarUtil();
                String cmd = "day";
                int y = 1990;
                int m = 10;
                int d = 17;


                c.setGregorian(y, m, d);
                c.computeChineseFields();
                c.computeSolarTerms();


                if (cmd.equalsIgnoreCase("year")) {
                        String[] t = c.getYearTable();
                        for (int i = 0; i < t.length; i++)
                                System.out.println(t[i]);
                } else if (cmd.equalsIgnoreCase("month")) {
                        String[] t = c.getMonthTable();
                        for (int i = 0; i < t.length; i++)
                                System.out.println(t[i]);
                } else {
                        System.out.println(c.toString());
                }
                System.out.println(c.getDateString());


        }


} 
